import prompt from '@system.prompt';

export default {
    data: {
        toast_text: null,
        display: "flex",
        animation: null,
        color_settings: ["#696969", "#006400", "#8b0000"], //dimgrey, darkgreen, darkred
        background_color: null,
        if_image: null,
        image_settings: ["/common/components/toast/images/exclamation_mark.png",
        "/common/components/toast/images/check_mark.png",
        "/common/components/toast/images/cross_mark.png"],
        image_src: null,
        div_top: null,
        div_height: null,
        toast_display: false,
        div_width: "100%",
        if_toast: true,
    },
    show_toast(text, toast_type, time) {
        let that = this;
        if (that.animation != null && that.if_toast == true) { //打断
            that.animation.finish();
            this.toast_display = false;
        }
        this.if_toast = true;
        that.toast_text = text;
        if (toast_type <= 2) {
            that.if_image = false;
            that.div_top = "90%"
            that.div_height = "40px"
            that.div_width = "80px"
            //            that.div_width = that.$element('toast_text').getBoundingClientRect().width + 40
        } else if (toast_type >= 3) {
            that.if_image = true;
            that.image_src = that.image_settings[toast_type % 3];
            that.div_top = "50%"
            that.div_height = "100px"
            that.div_width = "100px"
        }
        that.background_color = that.color_settings[toast_type % 3];
        var frames = [{
                          opacity: 1, offset: 0.0
                      }, {
                          opacity: 1, offset: 0.8
                      }, {
                          opacity: 0, offset: 1.0
                      }];
        var options = {
            duration: time,
            fill: "forwards",
        };
        this.animation = this.$element("toast_box").animate(frames, options); //通过id作用于div
        this.animation.onstart = function () {
            this.toast_display = true;
        };
        this.animation.onfinish = function () {
            this.toast_display = false;
            this.if_toast = false;
        };
        that.animation.play();
    },
}