export default {
    data: {},
    myFunction1() {
        this.$child('myToast').show_toast('正常', 0, 1000)
    },
    myFunction2() {
        this.$child('myToast').show_toast('正确', 1, 1000)
    },
    myFunction3() {
        this.$child('myToast').show_toast('错误', 2, 1000)
    },
    myFunction4() {
        this.$child('myToast').show_toast('正常', 3, 1000)
    },
    myFunction5() {
        this.$child('myToast').show_toast('正确', 4, 1000)
    },
    myFunction6() {
        this.$child('myToast').show_toast('错误', 5, 1000)
    },
}